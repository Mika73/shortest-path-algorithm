import json
from treeNode import TreeNode

def shortest_path(map, start, goal):
    row = len(map)
    column = len(map[0])
    previous_nodes = []

    node_name = '0'
    new_node = TreeNode(node_name, start)
    root_node = new_node
    previous_nodes.append(new_node)
    goal_node = ''

    while goal_node == '':
        new_nodes = []
        for previous_node in previous_nodes:

            # top left (can move right or down)
            if previous_node.data[0] == 0 and previous_node.data[1] == 0:
                new_nodes, goal_node = which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, True, False, False, True)

            # top right (can move left or down)
            elif previous_node.data[0]== 0 and previous_node.data[1]== column -1:
                new_nodes, goal_node = which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, False, True, False, True)

            # bottom left (can move right or up)
            elif previous_node.data[0]== row -1 and previous_node.data[1]== 0:
                new_nodes, goal_node = which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, True, False, True, False)

            # bottom right (can move left or up)
            elif previous_node.data[0]== row -1 and previous_node.data[1]== column -1:
                new_nodes, goal_node = which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, False, True, True, False)

            # top middle (can move right or left or down)
            elif previous_node.data[0]== 0:
                new_nodes, goal_node = which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, True, True, False, True)

            # bottom middle (can move right or left or up)
            elif previous_node.data[0]== row -1:
                new_nodes, goal_node = which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, True, True, True, False)

            # left middle (can move right or up or down)
            elif previous_node.data[1]== 0:
                new_nodes, goal_node = which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, True, False, True, True)

            # right middle (can move left or up or down)
            elif previous_node.data[1]== column -1:
                new_nodes, goal_node = which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, False, True, True, True)

            # middle (can move right or left or up or down)
            else:
                new_nodes, goal_node = which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, True, True, True, True)

        if new_nodes != []:
          previous_nodes = new_nodes
        else:
          goal_node = 'error'

    if goal_node == 'error':
      path_to_goal =[]
      steps = 0
    else:
      path_to_goal = goal_node.get_ancestors()
      path_to_goal.reverse()
      path_to_goal.append(goal_node.data)
      steps = goal_node.get_level()
    root_node.print_tree()
    return path_to_goal, steps

def which_direction_to_move(map, previous_nodes, new_nodes, previous_node, goal, goal_node, right, left, up, down):

  # move right
  if right == True:
    if map[previous_node.data[0]][previous_node.data[1]+1] == 1 and not already_passed(previous_node,[previous_node.data[0],previous_node.data[1]+1]):
      direction = 'right'
      new_nodes, goal_node = move(direction, previous_nodes, new_nodes, previous_node, goal, goal_node)

  # move left
  if left == True:
    if map[previous_node.data[0]][previous_node.data[1]-1] == 1 and not already_passed(previous_node,[previous_node.data[0],previous_node.data[1]-1]):
      direction = 'left'
      new_nodes, goal_node = move(direction, previous_nodes, new_nodes, previous_node, goal, goal_node)

  # move up
  if up == True:
    if map[previous_node.data[0]-1][previous_node.data[1]] == 1 and not already_passed(previous_node,[previous_node.data[0]-1,previous_node.data[1]]):
      direction = 'up'
      new_nodes, goal_node = move(direction, previous_nodes, new_nodes, previous_node, goal, goal_node)

  # move down
  if down == True:
    if map[previous_node.data[0]+1][previous_node.data[1]] == 1 and not already_passed(previous_node,[previous_node.data[0]+1,previous_node.data[1]]):
      direction = 'down'
      new_nodes, goal_node = move(direction, previous_nodes, new_nodes, previous_node, goal, goal_node)

  return new_nodes, goal_node

def already_passed(previous_node, next_node_data):
  ancestors = previous_node.get_ancestors()
  if next_node_data in ancestors:
    return True
  return False

def move(direction, previous_nodes, new_nodes, previous_node, goal, goal_node):
  if direction == 'left':
    new_node = TreeNode(str(int(previous_node.name) + 1),
                        [previous_node.data[0], previous_node.data[1]-1])
  if direction == 'right':
    new_node = TreeNode(str(int(previous_node.name) + 2),
                        [previous_node.data[0], previous_node.data[1]+1])
  if direction == 'up':
    new_node = TreeNode(str(int(previous_node.name) + 3),
                        [previous_node.data[0]-1, previous_node.data[1]])
  if direction == 'down':
    new_node = TreeNode(str(int(previous_node.name) + 4),
                        [previous_node.data[0]+1, previous_node.data[1]])

  previous_node.add_child(new_node)
  new_nodes.append(new_node)

  if new_node.data == goal:
      goal_node = new_node
  return new_nodes, goal_node

def draw_map(map, path_to_goal):
    row = len(map)
    column = len(map[0])

    #　Coordinate index
    output = ''
    for i in range(column):
      output += '     '
      output += str(i)
    print(output)

    #　Line
    output = '  '
    for i in range(column):
      output += '+-----'
    output += '+'
    print(output)

    for i in range(row):

      # Map data
      output = str(i) + ' |'
      for j in range(column):
        output += '  '
        output += str(map[i][j])
        output += '  |'
      print(output)

      # Path to goal
      output = '  |'
      for j in range(column):
        output += '  '
        if [i,j] == path_to_goal[0]:
          output += 'S'
        elif [i,j] == path_to_goal[-1]:
          output += 'G'
        elif [i,j] in path_to_goal:
          output += '*'
        else:
          output += ' '
        output += '  |'
      print(output)

      #　Line
      output = '  '
      for i in range(column):
        output += '+-----'
      output += '+'
      print(output)

json_open = open('map.json', 'r')
json_load = json.load(json_open)

path_to_goal, steps = shortest_path(json_load['map'], json_load['start'], json_load['goal'])
if steps > 0:
  print(f'Path to the goal: {path_to_goal}')
  print(f'Shortest way: {steps} steps')
  draw_map(json_load['map'], path_to_goal)
else:
  print('It\'s impossible to find the path from the start to the goal on the map provided')
