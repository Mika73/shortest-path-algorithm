# Shortest path algorithm
This program calculates the shortest route from an arbitrary start point to an arbitrary goal point by setting up passable and unpassable squares in a two-dimensional table lined with squares.

## How to use?
1. Edit Map
  * The map is described in the program as a two-dimensional array with a variable name of 'map'. The information in the map is 1 for passable and 0 for non-passable. You can change map information as you like.
2. Set the start and finish points.
  * The start and finish points are listed in the program. These can also be changed as you wish.
  * Both points are represented by the coordinates of the array.
  * The indexes start at 0. Coordinates are read from top to bottom and from left to right.
  * For example, the coordinates [2,4] represent the point located in the 5th column of the 3rd line
3. Run `python3 shortest.py`
4. The result will be displayed in the console

## Conditions
1. You can't take the same path twice.
2. If the route is not found, output an error message.
## How to set up a python environment
1. Setting Up Python 3
  * Ubuntu 20.04 and other versions of Debian Linux come with Python 3 preinstalled.
  * If you want to check the version of Python 3 `python3 -V`

## How did I come up with the shortest path algorithm?
![Hints from the maze](/note1.jpg "Hints from the maze")
1. I got the idea from how to solve the maze. Focusing on the fact that mazes always part ways, I tried to represent the pattern of paths after parting ways as a tree structure.
2. In order to realize the tree structure, I created the TreeNode class.
3. In the class, there are name, data, children, and parent properties . The data contains coordinates.
4. Each time the program advance one square on the grid map, it create a node. If there is more than one direction to go, the node will be divided into branches.
5. Repeat the process 4 until the goal is found, or until there is no more way to proceed.

## Struggles
![Creation of the tree](/note2.jpg "Creation of the tree")
1. I had a hard time creating the tree structure.
2. I tried to write the program recursively, but it was too difficult for me, so I just used a regular looping process.
3. Each time a new node object is created, the node object is stored in an array(new_nodes[]).
If the data of the newly created node object is the same as the goal point, or if no more new node objects can be created due to conditions such as the fact that the same path cannot be followed, the loop is terminated.
