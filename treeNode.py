class TreeNode:
    def __init__(self, name, data):
        self.name = name
        self.data = data
        self.children = []
        self.parent = None

    def get_level(self):
        level = 0
        p = self.parent
        while p:
            level += 1
            p = p.parent
        return level

    def print_tree(self):
        spaces = ' ' * self.get_level() * 3
        prefix = spaces + "|__" if self.parent else ""
        print(prefix + str(self.data))
        if self.children:
            for child in self.children:
                child.print_tree()

    def add_child(self, child):
        child.parent = self
        self.children.append(child)

    def get_ancestors(self):
      ancestors =[]
      while self.parent is not None:
        ancestors.append(self.parent.data)
        self = self.parent
      return ancestors
